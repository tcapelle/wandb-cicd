fastai==2.7.12
ipython==8.13.2
pandas==2.0.1
scikit_learn==1.2.2
torch==2.0.1
torchvision==0.15.2
wandb==0.15.2
pytest==7.3.1